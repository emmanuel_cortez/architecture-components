# Android Architecture Components #

El propósito de este tutorial es dar una introducción a la implementacion de Arquitectura MVVM usando los componentes arquitectónicos de Android.

## Ajustando nuestra configuración del proyecto ##

Para poder proseguir con nuestro proyecto es necesario:

- Implementar Java 8 como lenguaje de compilacion (disponible a partir de Android Studio 3.0.0).
- Elevar nuestra versión de construcción de Android al API 26.
- De igual manera, incrementar nuestra versión de las herramientas de construcción y de librerias de retro-compatibilidad a la version 26.1.0 como mínimo.

Ahora incluiremos las siguientes dependencias al archivo de construccion de Gradle:
 
       //Architecture Components
       //For Lifecycles
       implementation "android.arch.lifecycle:common-java8:1.0.0-rc1"
   
       //For LiveData, and ViewModel
       implementation "android.arch.lifecycle:extensions:1.0.0-rc1"
       
   
