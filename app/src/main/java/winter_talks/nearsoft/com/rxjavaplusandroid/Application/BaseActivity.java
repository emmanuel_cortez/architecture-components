package winter_talks.nearsoft.com.rxjavaplusandroid.Application;

import android.support.v7.app.AppCompatActivity;

import rx.Subscription;
import winter_talks.nearsoft.com.rxjavaplusandroid.rest.ApiClient;
import winter_talks.nearsoft.com.rxjavaplusandroid.rest.ApiInterface;

/**
 * Created by ecortez on 3/5/17.
 */

public class BaseActivity extends AppCompatActivity {
    // TODO - insert your themoviedb.org API KEY here
    protected final static String API_KEY = "7e8f60e325cd06e164799af1e317d7a7";
    private SubscriptionManager subscriptionManager = new SubscriptionManager();

    protected ApiInterface getApiService() {
        return ApiClient.getClient().create(ApiInterface.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscriptionManager.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        subscriptionManager.stop();
    }

    protected boolean bind(Subscription pSubscription) {
        return subscriptionManager.bind(pSubscription);
    }
}
