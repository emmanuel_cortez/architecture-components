package winter_talks.nearsoft.com.rxjavaplusandroid.Application;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ecortez on 3/8/17.
 */

class SubscriptionManager {
    private final CompositeSubscription compositeSubscription = new CompositeSubscription();
    private boolean isActive;

    public void start() {
        isActive = true;
    }

    public void stop() {
        isActive = false;
        compositeSubscription.clear();
    }

    public boolean bind(Subscription pSubscription) {
        if (isActive) {
            compositeSubscription.add(pSubscription);
        } else {
            pSubscription.unsubscribe();
        }
        return !pSubscription.isUnsubscribed();
    }

    public void remove(Subscription pSubscription) {
        compositeSubscription.remove(pSubscription);
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean hasSubscriptions() {
        return compositeSubscription.hasSubscriptions();
    }
}
