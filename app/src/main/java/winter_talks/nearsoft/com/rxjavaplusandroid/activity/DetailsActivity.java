package winter_talks.nearsoft.com.rxjavaplusandroid.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;
import com.squareup.picasso.Picasso;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import winter_talks.nearsoft.com.rxjavaplusandroid.Application.BaseActivity;
import winter_talks.nearsoft.com.rxjavaplusandroid.R;
import winter_talks.nearsoft.com.rxjavaplusandroid.model.Movie;

/**
 * Created by ecortez on 3/5/17.
 */

public class DetailsActivity extends BaseActivity {

    public static final String MOVIE_ID = "id";
    private static final String TAG = DetailsActivity.class.getSimpleName();

    LinearLayout moviesLayout;
    TextView movieTitle;
    TextView data;
    TextView movieDescription;
    TextView rating;
    Button tubeButton;
    ImageView poster;

    Movie movie;
    Integer id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        moviesLayout = (LinearLayout) findViewById(R.id.movies_layout);
        movieTitle = (TextView) findViewById(R.id.title);
        data = (TextView) findViewById(R.id.subtitle);
        movieDescription = (TextView) findViewById(R.id.description);
        rating = (TextView) findViewById(R.id.rating);
        tubeButton = (Button) findViewById(R.id.youtube_button);
        poster = (ImageView) findViewById(R.id.poster);

        id = getIntent().getIntExtra(MOVIE_ID, 0);

        if (id <= 0) {
            return;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribeGetMovieDetails();
        subscribeTubeButtonClick();
    }

    private void fillData(Movie movie) {
        this.movie = movie;
        movieTitle.setText(movie.getTitle());
        data.setText(movie.getReleaseDate());
        movieDescription.setText(movie.getOverview());
        rating.setText(movie.getVoteAverage().toString());

        Picasso.with(this).load(movie.getPosterPath()).into(poster);
    }

    private void subscribeGetMovieDetails() {
        if (movie == null) {
            bind(getApiService().getMovieDetails(id, API_KEY)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(movie -> fillData(movie)
                            , throwable -> Log.e(TAG, throwable.toString())));
        }
    }

    private void subscribeTubeButtonClick() {
        bind(RxView.clicks(tubeButton)
                .subscribe(aVoid -> {
                    String queryTitle = movie.getTitle().replace(" ", "+");
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/results?search_query=" + queryTitle)));
                })
        );
    }

}
