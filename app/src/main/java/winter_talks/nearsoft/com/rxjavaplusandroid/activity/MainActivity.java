package winter_talks.nearsoft.com.rxjavaplusandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import winter_talks.nearsoft.com.rxjavaplusandroid.Application.BaseActivity;
import winter_talks.nearsoft.com.rxjavaplusandroid.R;
import winter_talks.nearsoft.com.rxjavaplusandroid.adapter.MoviesAdapter;
import winter_talks.nearsoft.com.rxjavaplusandroid.model.Movie;
import winter_talks.nearsoft.com.rxjavaplusandroid.model.MoviesResponse;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private MoviesAdapter moviesAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (API_KEY.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please obtain your API KEY from themoviedb.org first!", Toast.LENGTH_LONG).show();
            return;
        }

        recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void fillData(MoviesResponse response) {
        List<Movie> movies = response.getResults();
        moviesAdapter = new MoviesAdapter(movies);
        recyclerView.setAdapter(moviesAdapter);

        subscribeRecyclerViewerItemClicks();
    }

    private void subscribeRecyclerViewerItemClicks() {
        if (moviesAdapter != null) {
            bind(moviesAdapter.getItemClicks()
                    .subscribe(movie -> {
                        Log.e(TAG, movie.getTitle() + ", " + movie.getId());

                        Intent details = new Intent(MainActivity.this, DetailsActivity.class);
                        details.putExtra(DetailsActivity.MOVIE_ID, movie.getId());

                        startActivity(details);
                    })
            );
        }
    }

    private void subscribeGetMovieList() {
        if (moviesAdapter == null) {
            bind(getApiService().getTopRatedMovies(API_KEY)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(moviesResponse -> fillData(moviesResponse)
                            , throwable -> Log.e(TAG, throwable.toString())));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribeGetMovieList();
        subscribeRecyclerViewerItemClicks();
    }
}
